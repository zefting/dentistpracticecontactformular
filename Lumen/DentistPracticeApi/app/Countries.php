<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ["created_at", "updated_at"];
}