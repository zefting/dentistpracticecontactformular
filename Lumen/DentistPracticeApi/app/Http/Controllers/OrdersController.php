<?php

namespace App\Http\Controllers;

use App\Orders;
use Illuminate\Http\Request;

class OrdersController extends Controller
{

    public function showAllOrders()
    {
        return response()->json(Orders::all());
    }
    public function showOneOrder($id)
    {
        return response()->json(Orders::find($id));
    }

    public function create(Request $request)
    {
        $order = Orders::create($request->all());

        return response()->json($order, 201);
    }

    public function update($id, Request $request)
    {
        $order = Orders::findOrFail($id);
        $order->update($request->all());

        return response()->json($order, 200);
    }

    public function delete($id)
    {
        Orders::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}