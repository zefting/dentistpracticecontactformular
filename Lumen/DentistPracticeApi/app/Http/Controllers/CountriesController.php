<?php

namespace App\Http\Controllers;

use App\Countries;
use Illuminate\Http\Request;

class CountriesController extends Controller
{

    public function showAllCountries()
    {
        return response()->json(Countries::all());
    }

}