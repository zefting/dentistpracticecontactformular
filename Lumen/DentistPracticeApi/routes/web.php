<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
$router->get('countries',  ['uses' => 'CountriesController@showAllCountries']);
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('orders',  ['uses' => 'OrdersController@showAllOrders']);
  
    $router->get('orders/{id}', ['uses' => 'OrdersController@showOneOrder']);
  
    $router->post('orders', ['uses' => 'OrdersController@create']);
  
    $router->delete('orders/{id}', ['uses' => 'OrdersController@delete']);
  
    $router->put('orders/{id}', ['uses' => 'OrdersController@update']);
  });
$router->group(['prefix' => 'api'], function () use ($router) {
   $router->get('contact',  ['uses' => 'ContactsController@showAllContacts']);
  
   $router->get('contact/{id}', ['uses' => 'ContactsController@showOneContact']);
  
   $router->post('contact', ['uses' => 'ContactsController@create']);
 
   $router->delete('contact/{id}', ['uses' => 'ContactsController@delete']);
  
   $router->put('contact/{id}', ['uses' => 'ContactsController@update']);
  });