import { Component, OnInit } from '@angular/core';
// http service

import {HttpClient, HttpHeaders} from '@angular/common/http';


@Component({
  selector: 'app-contactformular',
  templateUrl: './contactformular.component.html',
  styleUrls: ['./contactformular.component.css']
})
export class ContactformularComponent {
  SubmitContact: any;
  show = false;
  contactModel = {firm: null, email: null, comment: null };
  alerts: {
    type: 'success',
    message: 'This is an success alert'
  };
  constructor(private http: HttpClient) { }
  private mailScript = 'http://localhost:8000/api/contact';
  onSubmitSendEmail(){
    console.log('contactModel', this.contactModel);

    this.http.post<any>(this.mailScript, this.contactModel).subscribe(
      res => {
        console.log(res);
        this.show = true;
        console.log(this.show);
      });
  }

  onSubmit() {
    this.onSubmitSendEmail();
    console.log('submitted');
    this.contactModel = {firm: null, email: null, comment: null };
  }

}
