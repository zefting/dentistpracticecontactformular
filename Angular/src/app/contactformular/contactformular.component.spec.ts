import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactformularComponent } from './contactformular.component';

describe('ContactformularComponent', () => {
  let component: ContactformularComponent;
  let fixture: ComponentFixture<ContactformularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactformularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactformularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
