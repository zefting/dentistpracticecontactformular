import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ContactformularComponent} from './contactformular/contactformular.component';
import {OrderFormularComponent} from './order-formular/order-formular.component';
import {ServicesComponent} from './services/services.component';
import { PriceComponent} from './price/price.component';
const routes: Routes = [
{ path: 'contact', component: ContactformularComponent },
{ path: 'order', component: OrderFormularComponent },
{ path: 'services', component: ServicesComponent },
{ path: 'price', component: PriceComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
