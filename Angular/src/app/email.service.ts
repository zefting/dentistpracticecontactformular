import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class EmailService {
  constructor(private http: HttpClient) { }

  private mailScript = 'http://localhost:8000/api/contact';
  sendEmail(data: { firm: {}; email: {}; comment: {}; }): Observable<any>{
    return this.http.post<any>(this.mailScript, data);
}
}
