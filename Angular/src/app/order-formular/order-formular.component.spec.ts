import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderFormularComponent } from './order-formular.component';

describe('OrderFormularComponent', () => {
  let component: OrderFormularComponent;
  let fixture: ComponentFixture<OrderFormularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderFormularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderFormularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
