import { Component, OnInit } from '@angular/core';
// http service
import { DentistService } from '../dentist.service';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-order-formular',
  templateUrl: './order-formular.component.html',
  styleUrls: ['./order-formular.component.css']
})
export class OrderFormularComponent implements OnInit {
  form: FormGroup;
  Submitorder =
  {
    firstname: null,
    lastname: null,
    address: null,
    zip: null,
    email: null,
    firm: null,
    comment: null,
    bookingtype: 3,
    contry: null
  };

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  constructor(private dentistService: DentistService,  fb: FormBuilder, private httpClient: HttpClient) { }

  ngOnInit() {

     // get list of all countries
     this.dentistService.sendGetcountriesRequest().subscribe((data: any[]) => {
      console.log(data);
      this.dropdownList = data;
    });
     this.dropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }
  onItemSelect(item: any) {
    console.log(item);
    const itemKey = item.name;
    console.log(itemKey);
    this.Submitorder.contry = itemKey;
  }
  onSubmit() {
    console.log(this.Submitorder);
    this.httpClient.post<any>('http://localhost:8000/api/orders', this.Submitorder).subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );
    console.log('submitted', this.form);
  }


}
