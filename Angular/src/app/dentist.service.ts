import { Injectable } from '@angular/core';

// http
import { HttpClient, HttpHeaders } from '@angular/common/http';
// Http service

@Injectable({
  providedIn: 'root'
})
export class DentistService {

  private countriesApi = 'http://localhost:8000/api/countries';

  constructor(private httpClient: HttpClient) { }
  // requests a list of all countries
  public sendGetcountriesRequest(){
  return this.httpClient.get(this.countriesApi);
  }
}
